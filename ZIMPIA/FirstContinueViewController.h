//
//  FirstContinueViewController.h
//  ZIMPIA
//
//  Created by PPT on 5/26/17.
//  Copyright © 2017 PPT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NIDropDown.h"

@interface FirstContinueViewController : UIViewController
{
    IBOutlet UIButton *btnCountry;
    NIDropDown *dropDown;
}
@property (retain, nonatomic) IBOutlet UIButton *btnCountry;
- (IBAction)countryClicked:(id)sender;
- (void)rel;
- (IBAction)visaClicked:(id)sender;
- (IBAction)masterClicked:(id)sender;
- (IBAction)paypalClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *visaImage;
@property (strong, nonatomic) IBOutlet UIImageView *masterImage;
@property (strong, nonatomic) IBOutlet UIImageView *paypalImage;

@end
