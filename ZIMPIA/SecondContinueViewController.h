//
//  SecondContinueViewController.h
//  ZIMPIA
//
//  Created by PPT on 5/27/17.
//  Copyright © 2017 PPT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NIDropDown.h"

@interface SecondContinueViewController : UIViewController
{
    IBOutlet UIButton *btnCountry;
    NIDropDown *dropDown;
}
@property (retain, nonatomic) IBOutlet UIButton *btnCountry;
- (IBAction)countryClicked:(id)sender;
- (void)rel;
@end
