//
//  FirstContinueViewController.m
//  ZIMPIA
//
//  Created by PPT on 5/26/17.
//  Copyright © 2017 PPT. All rights reserved.
//

#import "FirstContinueViewController.h"

@interface FirstContinueViewController ()

@end

@implementation FirstContinueViewController
@synthesize btnCountry;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
    
   
}
- (void)viewDidUnload {
    //    [btnSelect release];
    btnCountry = nil;
    [self setBtnCountry:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (void)dealloc {
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

- (IBAction)countryClicked:(id)sender {
    
        NSArray * arr = [[NSArray alloc] init];
        arr = [NSArray arrayWithObjects:@"JAPAN", @"RUSSIA", @"TURKEY", @"GREECE", @"AUSTRALIA", @"INDIA", @"TAIWAN", @"SINGAPORE", @"MALAYSIA", @"BRAZIL",nil];
        NSArray * arrImage = [[NSArray alloc] init];
        
        if(dropDown == nil) {
            CGFloat f = 200;
            dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :arrImage :@"down"];
            dropDown.delegate = self;
        }
        else {
            [dropDown hideDropDown:sender];
            [self rel];
        }
}
- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    [self rel];
    NSLog(@"%@", btnCountry.titleLabel.text);
}

-(void)rel{
    //    [dropDown release];
    dropDown = nil;
}

- (IBAction)visaClicked:(id)sender {
    
    _visaImage.image = [UIImage imageNamed:@"radio_sel_button.png"];
    _masterImage.image = [UIImage imageNamed:@"radio_button.png"];
    _paypalImage.image = [UIImage imageNamed:@"radio_button.png"];
}

- (IBAction)masterClicked:(id)sender {
    _visaImage.image = [UIImage imageNamed:@"radio_button.png"];
    _masterImage.image = [UIImage imageNamed:@"radio_sel_button.png"];
    _paypalImage.image = [UIImage imageNamed:@"radio_button.png"];
}

- (IBAction)paypalClicked:(id)sender {
    _visaImage.image = [UIImage imageNamed:@"radio_button.png"];
    _masterImage.image = [UIImage imageNamed:@"radio_button.png"];
    _paypalImage.image = [UIImage imageNamed:@"radio_sel_button.png"];
}

@end
